package BP.Frm;

import BP.DA.*;
import BP.En.*;
import BP.WF.Port.*;
import java.util.*;

/** 
 单据可创建的工作岗位属性	  
*/
public class StationCreateAttr
{
	/** 
	 单据
	*/
	public static final String FrmID = "FrmID";
	/** 
	 工作岗位
	*/
	public static final String FK_Station = "FK_Station";
}
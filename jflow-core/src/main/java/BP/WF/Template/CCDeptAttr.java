package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Port.*;
import BP.WF.*;
import java.util.*;

/** 
 抄送部门属性	  
*/
public class CCDeptAttr
{
	/** 
	 节点
	*/
	public static final String FK_Node = "FK_Node";
	/** 
	 工作部门
	*/
	public static final String FK_Dept = "FK_Dept";
}